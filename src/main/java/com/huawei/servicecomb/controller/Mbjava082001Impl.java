package com.huawei.servicecomb.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-08-20T14:51:22.018Z")

@RestSchema(schemaId = "mbjava082001")
@RequestMapping(path = "/rest", produces = MediaType.APPLICATION_JSON)
public class Mbjava082001Impl {

    @Autowired
    private Mbjava082001Delegate userMbjava082001Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userMbjava082001Delegate.helloworld(name);
    }

}
