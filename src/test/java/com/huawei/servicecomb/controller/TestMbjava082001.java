package com.huawei.servicecomb.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestMbjava082001 {

        Mbjava082001Delegate mbjava082001Delegate = new Mbjava082001Delegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = mbjava082001Delegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}